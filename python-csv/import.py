from pathlib import Path
import csv
import sys
import os

try:
    path = Path(os.getcwd(), 'src', 'datos-src.csv')
    file = open(path)
    csvreader = csv.reader(file)
    header = next(csvreader)
except:
    print(f'No se pudo leer el archivo, error fatal!')
    sys.exit(1)

# Declaramos la tupla, donde guardaremos las lineas
rows = []

for row in csvreader:
    for x in range(0, len(row), 1):
        try:
            if not row[x]:
                raise Exception("No pueden existir campos nulos o en blanco")
            else:
                row[x] = row[x].strip()
        except Exception as e:
            print(f'{e}, error en la fila {csvreader.line_num}')
            rows = []
            sys.exit(1)
    
    rows.append(row)


file.close()

path = Path(os.getcwd(), 'dest', 'datos-dest.csv')
with open(path, 'w', newline="") as file:
    csvwriter = csv.writer(file)
    csvwriter.writerow(header)
    csvwriter.writerows(rows)


print(f'Trabajo realizado con exito')